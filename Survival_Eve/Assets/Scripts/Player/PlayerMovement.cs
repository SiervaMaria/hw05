﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //언제든지 조절 할 수 있는 공개변수설정.
    public float speed = 6f;

    //참조할 변수들을 설정한다 private.

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;

    /*아래는 사전에 설정 해 두었던 레이캐스트를 위한 floor마스크이다.
      레이어 설정을 통해 관리할 것이다 레이캐스트가 floor와만 충돌할 수
      있도록 레이어마스크를 이용한 것이다.*/

    int floorMask;

    /*아래는 카메라에서 발사되는 광선의 길이를 설정한 것이다.*/

    float camRayLength = 100f;

    /*아래는 Start()와 유사하지만 스크립트 활성과는 관계없이 호출된다.
    따라서 참조 설정 등에 유리하다고 한다.*/

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    
    }

    //아래는 캐릭터를 물리효과와함께 움직이는 함수이다.

    void FixedUpdate()
    {
        /*GetAxisRaw의 Raw축은 -1과0과1의값만 가지며 세숫자사이의 값은
         * 가지지 않는다. 일반축의 값은 -1에서 1사이이다. 서서히움직이는것이 아니라
         곧바로 최고속도로 움직이기 때문에 반응성이 뛰어난 움직임을 얻을 수 있다. */
        float h = Input.GetAxisRaw("Horizontal"); //수평값.
        float v = Input.GetAxisRaw("Vertical"); //수평값.

        //아래는 PlyerMovement 스크립트에 작성한 함수들을 실제 사용하기위해 호출하는 명령어이다.

        Move(h, v);
        Turning();
        Animating(h, v);

    }

    //캐릭터가 실제 움직이는 것을 설정.
    void Move(float h,  float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);

    }



    void Turning()
    {

        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if(Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);

        }
    }


    void Animating (float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);

    }







}
